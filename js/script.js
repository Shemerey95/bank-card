//  The validation function of the field "card Number":
let cardInputs = document.querySelectorAll('.page-main__box-number input');

let mas = [false, false, false, false];
for (let i = 0; i < cardInputs.length; i++) {
    cardInputs[i].i = i;
    // Hang the handler on the input event:
    cardInputs[i].addEventListener('input', function () {
        // Create a variable and save an instance of the regular expression class to it
        var reg = new RegExp('^\\d+$');
        if (reg.test(this.value) && this.value.length === 4) {
            this.classList.remove('notValid');
            mas[this.i] = true;
        } else  {
            this.classList.add('notValid');
            mas[this.i] = false;
        }
        checkForm();
    })
}


//  The validation function of the field "card Holder":
let ownerCard = false;

document.getElementById('page-main__owner-card').addEventListener('input', function() {
    let leg = new RegExp('^[a-zA-Z]+$');
    if (leg.test(this.value) && this.value.length >= 4) {
        this.classList.remove('notValid');
        ownerCard = true;
    } else {
        this.classList.add('notValid');
        ownerCard  = false;
    }
    checkForm();
});


//  The validation function of the field "Code CVV2/CVC2":
let cvv = false;

document.querySelector('.page-main__code input').addEventListener('input', function() {
    let zeg = new RegExp('^\\d+$');
    if (zeg.test(this.value) && this.value.length === 3) {
        this.classList.remove('notValid');
        cvv = true;
    } else {
        this.classList.add('notValid');
        cvv  = false;
    }
    checkForm();
});


//  Function to unlock the "Send" button":
function checkForm() { 
    let result = false;
    if (mas.every(function(element) {
        return element === true;
    }) && ownerCard && cvv) result = true;
        else result = false;
    if (result === true) {
         document.getElementById('page-main__button').disabled = false;
         document.getElementById('page-main__button').classList.add('opacity');
    }
    else {
        document.getElementById('page-main__button').disabled = true;
        document.getElementById('page-main__button').classList.remove('opacity');
    }
}


//  Function for the function of the button "Menu" on mobile devices and tablets:
let btn = document.querySelector('.mobile-menu__button');
let blockHidden = document.querySelector('.aside-links');

function showMenu() {
    blockHidden.classList.toggle('visible');
}

btn.addEventListener('click', showMenu);
